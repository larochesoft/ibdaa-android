package com.larochesoft.ibdaa;


import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
 
public class ReportsWebViewActivity extends Activity {
 
	private WebView webView;
	public static String URL;
	
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
               finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_webview);
 
		
		  ActionBar actionBar = getActionBar();
	        actionBar.setDisplayHomeAsUpEnabled(true);
	        
		webView = (WebView) findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBlockNetworkLoads(false);
        webView.setWebViewClient(new WebViewClient());

        webView.getSettings().setPluginsEnabled(true);


		webView.loadUrl(ReportsWebViewActivity.URL);
		//"http://103.1.173.72/Reports_SQL08/Pages/Report.aspx?ItemPath=%2fAppointmentsReports%2fPersons");
		
		

 
	}
 
	
}