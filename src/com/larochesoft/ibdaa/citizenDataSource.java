package com.larochesoft.ibdaa;


import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class citizenDataSource {


	  // Database fields
	  private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	  private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
	      MySQLiteHelper.COLUMN_Name, 
	      MySQLiteHelper.COLUMN_FatherName, 
	      MySQLiteHelper.COLUMN_LastName, 
	      MySQLiteHelper.COLUMN_MotherName, 
	      MySQLiteHelper.COLUMN_Gender, 
	      MySQLiteHelper.COLUMN_SpouseName, 
	      MySQLiteHelper.COLUMN_SpouseWork, 
	      MySQLiteHelper.COLUMN_RakemElSejel, 
	      MySQLiteHelper.COLUMN_MahllatElSejel, 
	      MySQLiteHelper.COLUMN_Mobile,
	      MySQLiteHelper.COLUMN_CurrentWork,
	      MySQLiteHelper.COLUMN_ProjectName,  
	      MySQLiteHelper.COLUMN_Marital, 
	      MySQLiteHelper.COLUMN_DOB,
	      MySQLiteHelper.COLUMN_Status,
	      MySQLiteHelper.COLUMN_OraId,
	      MySQLiteHelper.COLUMN_ProductId,
	      MySQLiteHelper.COLUMN_BranchCode,
	      MySQLiteHelper.COLUMN_AreaSupervisor,
	      MySQLiteHelper.COLUMN_LoanSupervisor,
	      MySQLiteHelper.COLUMN_UserId,
	      MySQLiteHelper.COLUMN_LoanAmount,
	      MySQLiteHelper.COLUMN_PotLoanAmount,
	      MySQLiteHelper.COLUMN_Freq,
	      MySQLiteHelper.COLUMN_AppStatus,
	      MySQLiteHelper.COLUMN_AreaCode,
	      MySQLiteHelper.COLUMN_CityCode,
	      MySQLiteHelper.COLUMN_SuburbCode
	      };

	  public citizenDataSource(Context context) {
	    dbHelper = new MySQLiteHelper(context);
	  }

	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }

	  public void close() {
	    dbHelper.close();
	  }

	 // public int getUnProcessedCount()
	  //{
		    //return database.query(false, MySQLiteHelper.TABLE_CITIZEN, columns, selection, selectionArgs, groupBy, having, orderBy, limit)
	  //}
	  
	  public int getUnProcessedCount() {
		    int count = 0;

		    Cursor cursor = database.rawQuery(
		        "select count(*) from CITIZEN where status=0",
		        new String[]{});
		    cursor.moveToFirst();
		    count = cursor.getInt(0);   
		    return count;
		} 
	
	  public int updateAppStatus(long citizenId, int status)
	  {
		  ContentValues val = new ContentValues();
		    val.put(MySQLiteHelper.COLUMN_AppStatus, status);
		    
		    return database.update(MySQLiteHelper.TABLE_CITIZEN, val, MySQLiteHelper.COLUMN_ID + "=?", new String[]{String.valueOf(citizenId)});  
	  }
	  
	  
	  public int updateStatus(long citizenId, int status, long OraId)
	  {
		  ContentValues val = new ContentValues();
		    val.put(MySQLiteHelper.COLUMN_Status, status);
		    val.put(MySQLiteHelper.COLUMN_OraId, OraId);
		    return database.update(MySQLiteHelper.TABLE_CITIZEN, val, MySQLiteHelper.COLUMN_ID + "=?", new String[]{String.valueOf(citizenId)});  
	  }
	  public int updateApplicationData(long citizenId, String currentWork, String projectName, int productId, int branchCode, int areaSupervisor, int loanSupervisor, 
			  int userId, float loanAmount, float potAmount, int freq)
	  {
		  ContentValues val = new ContentValues();
		  	val.put(MySQLiteHelper.COLUMN_CurrentWork, currentWork);
		  	val.put(MySQLiteHelper.COLUMN_ProjectName, projectName);
		    val.put(MySQLiteHelper.COLUMN_ProductId, productId);
		    val.put(MySQLiteHelper.COLUMN_BranchCode, branchCode);
		    val.put(MySQLiteHelper.COLUMN_AreaSupervisor, areaSupervisor);
		    val.put(MySQLiteHelper.COLUMN_LoanSupervisor, loanSupervisor);
		    val.put(MySQLiteHelper.COLUMN_UserId, userId);
		    val.put(MySQLiteHelper.COLUMN_LoanAmount, loanAmount);
		    val.put(MySQLiteHelper.COLUMN_PotLoanAmount, potAmount);
		    val.put(MySQLiteHelper.COLUMN_Freq, freq);
		    return database.update(MySQLiteHelper.TABLE_CITIZEN, val, MySQLiteHelper.COLUMN_ID + "=?", new String[]{String.valueOf(citizenId)});  
	  }
	  public citizen createCitizen( String Name,
			    String FatherName,
			    String LastName,
			    String MotherName,
			    int Gender,
			    String SpouseName,
			    String SpouseWork,
			    String RakemElSejel,
			    String MahllatElSejel,
			    String Mobile,
			    String CurrentWork,
			    String ProjectName,
			    int Marital,
			    String DOB,
			    int Status, int OraId,int productId, int branchCode, int areaSupervisor, int loanSupervisor, 
				  int userId, float loanAmount, float potAmount, int freq, String areaCode, String cityCode, String suburbCode) {
	    ContentValues values = new ContentValues();
	    values.put(MySQLiteHelper.COLUMN_CurrentWork, CurrentWork);
	    values.put(MySQLiteHelper.COLUMN_DOB, DOB);
	    values.put(MySQLiteHelper.COLUMN_FatherName, FatherName);
	    values.put(MySQLiteHelper.COLUMN_Gender, Gender);
	    values.put(MySQLiteHelper.COLUMN_LastName, LastName);
	    values.put(MySQLiteHelper.COLUMN_MahllatElSejel, MahllatElSejel);
	    values.put(MySQLiteHelper.COLUMN_Marital, Marital);
	    values.put(MySQLiteHelper.COLUMN_Mobile, Mobile);
	    values.put(MySQLiteHelper.COLUMN_MotherName, MotherName);
	    values.put(MySQLiteHelper.COLUMN_Name, Name);
	    values.put(MySQLiteHelper.COLUMN_ProjectName, ProjectName);
	    values.put(MySQLiteHelper.COLUMN_RakemElSejel, RakemElSejel);
	    values.put(MySQLiteHelper.COLUMN_SpouseName, SpouseName);
	    values.put(MySQLiteHelper.COLUMN_SpouseWork, SpouseWork);
	    values.put(MySQLiteHelper.COLUMN_Status, Status);
	    values.put(MySQLiteHelper.COLUMN_OraId, OraId);
	    values.put(MySQLiteHelper.COLUMN_ProductId, productId);
	    values.put(MySQLiteHelper.COLUMN_BranchCode, branchCode);
	    values.put(MySQLiteHelper.COLUMN_AreaSupervisor, areaSupervisor);
	    values.put(MySQLiteHelper.COLUMN_LoanSupervisor, loanSupervisor);
	    values.put(MySQLiteHelper.COLUMN_UserId, userId);
	    values.put(MySQLiteHelper.COLUMN_LoanAmount, loanAmount);
	    values.put(MySQLiteHelper.COLUMN_PotLoanAmount, potAmount);
	    values.put(MySQLiteHelper.COLUMN_Freq, freq);
	    values.put(MySQLiteHelper.COLUMN_AreaCode, areaCode);
	    values.put(MySQLiteHelper.COLUMN_CityCode, cityCode);
	    values.put(MySQLiteHelper.COLUMN_SuburbCode, suburbCode);
	    long insertId = database.insert(MySQLiteHelper.TABLE_CITIZEN, null,
	        values);
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
	        allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
	        null, null, null);
	    cursor.moveToFirst();
	    citizen newCitizen = cursorToCitizen(cursor);
	    cursor.close();
	    return newCitizen;
	  }

	  public void deleteCitizen(citizen citizen1) {
	    long id = citizen1.CITIZEN_ID;
	    System.out.println("Citizen deleted with id: " + id);
	    database.delete(MySQLiteHelper.TABLE_CITIZEN, MySQLiteHelper.COLUMN_ID
	        + " = " + id, null);
	  }

	  public List<citizen> getUnProcessedCitizens() {
		    List<citizen> comments = new ArrayList<citizen>();
		    
		    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
		        allColumns, MySQLiteHelper.COLUMN_Status +"=?", new String[]{"0"}, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      citizen citizen1 = cursorToCitizen(cursor);
		      comments.add(citizen1);
		      cursor.moveToNext();
		    }
		    // Make sure to close the cursor
		    cursor.close();
		    return comments;
		  }
	  
	  public List<citizen> getAllCitizens() {
	    List<citizen> comments = new ArrayList<citizen>();
	    Log.d("ibdaaDY", database.getPath());
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
	        allColumns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      citizen citizen1 = cursorToCitizen(cursor);
	      comments.add(citizen1);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return comments;
	  }

	  
	  public List<citizen> getAllPendingCitizens() {
		    List<citizen> comments = new ArrayList<citizen>();
		    Log.d("ibdaaDY", database.getPath());
		    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
		        allColumns, "status=0", null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      citizen citizen1 = cursorToCitizen(cursor);
		      comments.add(citizen1);
		      cursor.moveToNext();
		    }
		    
		    
		    // Make sure to close the cursor
		    cursor.close();
		    return comments;
		  }
	  public List<citizen> getAllPendingApps() {
		    List<citizen> comments = new ArrayList<citizen>();
		    Log.d("ibdaaDY", database.getPath());
		    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
		        allColumns, "AppStatus=0", null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      citizen citizen1 = cursorToCitizen(cursor);
		      comments.add(citizen1);
		      cursor.moveToNext();
		    }
		    
		    
		    // Make sure to close the cursor
		    cursor.close();
		    return comments;
		  }

	  public citizen getCitizenById(Long CitizenId) {
		    List<citizen> comments = new ArrayList<citizen>();
		   
		    Cursor cursor = database.query(MySQLiteHelper.TABLE_CITIZEN,
		        allColumns, MySQLiteHelper.COLUMN_ID+"="+String.valueOf(CitizenId), null, null, null, null);
		    citizen citizen1 = new citizen();
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      citizen1 = cursorToCitizen(cursor);
		      comments.add(citizen1);
		      cursor.moveToNext();
		    }
		    // Make sure to close the cursor
		    cursor.close();
		    return citizen1;
		  }
	  private citizen cursorToCitizen(Cursor cursor) {
	    citizen citizen1 = new citizen();
	    citizen1.CITIZEN_ID= cursor.getLong(0);
	    citizen1.Name=cursor.getString(1);
	    citizen1.FatherName=cursor.getString(2);
		  citizen1.LastName =cursor.getString(3);
		  citizen1.MotherName = cursor.getString(4);
		  citizen1.Gender = cursor.getInt(5);
		  citizen1.SpouseName = cursor.getString(6);
		  citizen1.SpouseWork = cursor.getString(7);
		  citizen1.RakemElSejel = cursor.getString(8);
		  citizen1.MahllatElSejel = cursor.getString(9);
		  citizen1.Mobile = cursor.getString(10);
		  citizen1.CurrentWork = cursor.getString(11);
		  citizen1.ProjectName = cursor.getString(12);
		  citizen1.Marital = cursor.getInt(13);
		  citizen1.DOB = cursor.getString(14);
		  citizen1.Status = cursor.getInt(15);
		  citizen1.OraId=cursor.getLong(16);
		  citizen1.ProductId=cursor.getInt(17);
		  citizen1.BranchCode=cursor.getInt(18);
		  citizen1.AreaSupervisor=cursor.getInt(19);
		  citizen1.LoanSupervisor=cursor.getInt(20);
		  citizen1.UserId=cursor.getInt(21);
		  citizen1.LoanAmount=cursor.getFloat(22);
		  citizen1.PotLoanAmount=cursor.getFloat(23);
		  citizen1.Freq=cursor.getInt(24);
		  citizen1.AppStatus=cursor.getInt(25);
		  citizen1.AreaCode=cursor.getString(26);
		  citizen1.CityCode=cursor.getString(27);
		  citizen1.SuburbCode=cursor.getString(28);
	    return citizen1;
	  }
}
