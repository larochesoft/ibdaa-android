package com.larochesoft.ibdaa;

import java.util.Calendar;
import java.util.List;


import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;
import android.support.v4.app.NavUtils;

public class Ibdaa extends Activity {

	 
	private Menu m;
	  @Override
	     public boolean onCreateOptionsMenu(Menu menu) {
	     	 MenuInflater inflater = getMenuInflater();
	     	    inflater.inflate(R.menu.main_menu, menu);
	     	   
	     	   
	     	    return true;
	        
	     }
	     
	     @Override
	     public boolean onPrepareOptionsMenu(Menu menu) {
	    	 
	         MenuItem item= menu.findItem(R.id.menu_status);
	         startService(new Intent(this, LoginSyncService.class));
             startService(new Intent(this, SyncService.class));
             startService(new Intent(this, AppSyncService.class));
	         //depending on you conditions, either enable/disable
	         citizenDataSource datasource = new citizenDataSource(this);
	     	 datasource.open();
	     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
	     	 datasource.close();
	     	 
	     	 item= menu.findItem(R.id.menu_user);
	     	item.setTitle(SplashScreenActivity.username);
	     	 m=menu;
	     	 
	     	 
	         return super.onPrepareOptionsMenu(menu);
	     }
	     
	     
	     @Override
	     public boolean onOptionsItemSelected(MenuItem item) {
	         switch (item.getItemId()) {
	             case android.R.id.home:
	                 // app icon in action bar clicked; go home
	                finish();
	                 return true;
	             case R.id.menu_status:
	            	 startService(new Intent(this, LoginSyncService.class));
	                 startService(new Intent(this, SyncService.class));
	                 startService(new Intent(this, AppSyncService.class));
	            	 DialogInterface.OnClickListener dialogClickListener1 = new DialogInterface.OnClickListener() {
	         		    @Override
	         		    public void onClick(DialogInterface dialog, int which) {
	         		    	
	         		    }
	            	 };
	            	 
	            	 AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
	            	 
	            	 citizenDataSource datasource = new citizenDataSource(this);
	             	 datasource.open();
	             	 String values="";
	             	List<citizen> cList = datasource.getAllCitizens();
	             	 for (int i=cList.size()-1; i>=0;i--)
	             	 {
	             	
	             		 citizen c=cList.get(i);
	             		values+="Name:"+c.Name+" "+c.FatherName+" "+c.LastName +"\tLId:"+String.valueOf(c.CITIZEN_ID)
	            				 +"\tRId:"+String.valueOf(c.OraId)+"\tStatus:"+String.valueOf(c.Status)+"\n";
	             	 }
	             	 datasource.close();
	         		builder1.setMessage(values).setPositiveButton("Yes", dialogClickListener1)
	         		    .setNegativeButton("No", dialogClickListener1).show();
	            	
	                 return true;
	             default:
	                 return super.onOptionsItemSelected(item);
	         }
	     }
	     

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        startService(new Intent(this, LoginSyncService.class));
        startService(new Intent(this, SyncService.class));
        startService(new Intent(this, AppSyncService.class));
        
     
        
        
        Handler handler = new Handler();
        
        // run a thread after 2 seconds to start the home screen
        handler.postDelayed(new Runnable() {
 
            @Override
            public void run() {
 
                // make sure we close the splash screen so the user won't come back when it presses back key
 
             
               
               if (m!=null)
               {
            	  
            	   MenuItem item= m.findItem(R.id.menu_status);
      	         //depending on you conditions, either enable/disable
      	         citizenDataSource datasource = new citizenDataSource(getApplicationContext());
      	     	 datasource.open();
      	     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
      	     	 datasource.close();
               }
            }
 
        }, 10000); // time in milliseconds (1 second = 1000 milliseconds) until the run() method will be called
 
      
/*
        TabHost tabHost = getTabHost();
 
        // Tab for Photos
        TabSpec clientInfoSpec = tabHost.newTabSpec("Client Info");
        // setting Title and Icon for the Tab
        clientInfoSpec.setIndicator("العميل", getResources().getDrawable(R.drawable.ic_action_search));
        Intent clientInfoIntent = new Intent(this, ClientInfoActivity.class);
        clientInfoSpec.setContent(clientInfoIntent);
 
        
        TabSpec applicationSpec = tabHost.newTabSpec("Application Info");
        // setting Title and Icon for the Tab
        applicationSpec.setIndicator("الطلب", getResources().getDrawable(R.drawable.ic_action_search));
        Intent applicationIntent = new Intent(this, ApplicationActivity.class);
        applicationSpec.setContent(applicationIntent);
        
        TabSpec searchSpec = tabHost.newTabSpec("Search");
        // setting Title and Icon for the Tab
        searchSpec.setIndicator("البحث", getResources().getDrawable(R.drawable.ic_action_search));
        Intent searchIntent= new Intent(this, SearchActivity.class);
        searchSpec.setContent(searchIntent);
        
        
        TabSpec reportsSpec = tabHost.newTabSpec("Reports");
        // setting Title and Icon for the Tab
        reportsSpec.setIndicator("التقارير", getResources().getDrawable(R.drawable.ic_action_search));
        Intent reportsIntent = new Intent(this, ReportsActivity.class);
        reportsSpec.setContent(reportsIntent);
        
        
        tabHost.addTab(clientInfoSpec); // Adding videos tab
        tabHost.addTab(applicationSpec);
        tabHost.addTab(searchSpec); // Adding videos tab
        tabHost.addTab(reportsSpec);*/
        
        
        
        ImageButton btnNewApplication = (ImageButton)findViewById(R.id.btnNewApplication);
        btnNewApplication.setOnClickListener(new OnClickListener() 
        {   public void onClick(View v) 
            {   
                Intent intent = new Intent(Ibdaa.this, ClientInfoActivity.class);
                startActivity(intent);      
                //finish();
            }
        });
        
        ImageButton btnSearch = (ImageButton)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new OnClickListener() 
        {   public void onClick(View v) 
            {   
        	
        	String uri = "geo:33.8886289,35.4954794?q=33.895137,35.473861";
        	Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        	intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        	startActivity(intent);
        	
                //Intent intent = new Intent(Ibdaa.this, SearchActivity.class);
                //startActivity(intent);      
                //finish();
            }
        });
        
        
        ImageButton btnReports = (ImageButton)findViewById(R.id.btnReports);
        btnReports.setOnClickListener(new OnClickListener() 
        {   public void onClick(View v) 
            {   
        	if (Integer.parseInt(SplashScreenActivity.userid)!=0)
        	{
                Intent intent = new Intent(Ibdaa.this, ReportsActivity.class);
                startActivity(intent);      
        	}
        	else
        	{
        		Toast.makeText(Ibdaa.this, "No active user!", Toast.LENGTH_LONG);
        	}
                //finish();
            }
        });
        
        
        
    }

    
   
    
}
