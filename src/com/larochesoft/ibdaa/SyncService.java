package com.larochesoft.ibdaa;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SyncService extends Service {
	 @Override
	    public IBinder onBind(Intent arg0) {
	        // TODO Auto-generated method stub
	        return null;
	    }
	 
	 
	 public  boolean isOnline() {
		    ConnectivityManager cm =
		        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    return false;
		}
	 
	 public static String connect(String url, String CitizenId)
     {
		 try{

 	    	if (InetAddress.getByName(SplashScreenActivity.baseAddress).isReachable(10000))
 	    	{
         HttpClient httpclient = new DefaultHttpClient();
         String result = "";
         // Prepare a request object
         HttpGet httpget = new HttpGet(url); 

         // Execute the request
         HttpResponse response;
         try {
             response = httpclient.execute(httpget);
             // Examine the response status
             Log.i("Praeda",response.getStatusLine().toString());

             // Get hold of the response entity
             HttpEntity entity = response.getEntity();
             // If the response does not enclose an entity, there is no need
             // to worry about connection release

             if (entity != null) {

                 // A Simple JSON Response Read
                 InputStream instream = entity.getContent();
                  result= convertStreamToString(instream);
                 // now you have the string representation of the HTML request
                 instream.close();
             }


         } catch (Exception e) {
        	 
        	 return "error"+e.toString();
         }
         return result.trim() + "&"+CitizenId.trim();
 	    	 }
		 }
		    	catch(Exception ex)
		    	{
		    		
		    	}
		    	
		    	return "error: connectivity";
     }

         private static String convertStreamToString(InputStream is) {
         /*
          * To convert the InputStream to String we use the BufferedReader.readLine()
          * method. We iterate until the BufferedReader return null which means
          * there's no more data to read. Each line will appended to a StringBuilder
          * and returned as String.
          */
         BufferedReader reader = new BufferedReader(new InputStreamReader(is));
         StringBuilder sb = new StringBuilder();

         String line = null;
         try {
             while ((line = reader.readLine()) != null) {
                 sb.append(line + "\n");
             }
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 is.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
         return sb.toString();
     }
         
         private class CallWebRequestTask extends AsyncTask<Object, Object, Object> {
             protected String doInBackground(Object... urls) {
            	
                 return connect((String)urls[0],urls[1].toString());
             }

             protected void onPostExecute(Object result1) {
            	 Context context = getApplicationContext();
            	 int duration = Toast.LENGTH_LONG;
            	 
            	String result = ((String)result1).split("&")[0];
            	 citizenDataSource datasource = new citizenDataSource(context);
            	 datasource.open();
            	 if (((String)result).contains("LS_error"))
            	 {
            		 long citizenId = Long.parseLong(((String)result1).split("&")[1]);
            		 
            		 datasource.updateStatus(citizenId, 1, 0);
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            	 }
            	 else if (((String)result).contains("LS_success"))
            	 {
            		 long citizenId = Long.parseLong(((String)result1).split("&")[1]);
            		 
            		 datasource.updateStatus(citizenId, 1, Long.parseLong(  ((String)result).trim().split("_")[1]));
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            	 }
            	 else 
            	 {
            		 
            	//	Toast toast = Toast.makeText(context, "Connectivity issue, saving locally!", duration);
                 //	toast.show();
            	 }
            	
            	 datasource.close();
            	 
             }

			
         }
         
	  @Override
	    public void onStart(Intent intent, int startId) {
	        // TODO Auto-generated method stub
	        super.onStart(intent, startId);
	        
	        citizenDataSource datasource = new citizenDataSource(this);
	     	 datasource.open();
	     	 List <citizen> pendingCitizens=datasource.getAllPendingCitizens();
	     	 for (int i=0; i<pendingCitizens.size(); i++)
	     	 {
	     		citizen cit = pendingCitizens.get(i);
	     		
	     		String encodedUrl="";
	    		try {
	    			
	    			String reg2="";
	    			String regPlace="";
	    			
	    				reg2=URLEncoder.encode(cit.MahllatElSejel,"utf-8");
	    			String query=SplashScreenActivity.baseUrl+"AddData.aspx?STP=MOBILE_APP_CITIZEN&p_M_TYPE=1&P_name1="+
	    					URLEncoder.encode(cit.Name,"utf-8")+"&P_name2="+
	    					URLEncoder.encode(cit.FatherName,"utf-8")+"&P_name4="+
	    					URLEncoder.encode(cit.LastName,"utf-8")+"&P_BIRTH_DATE=2012-01-01%2000:00:00&"+
	    					"P_GUAR_FLAG=2&P_GENDER="+cit.Gender+
	    					"&P_MARITAL_STATUS="+String.valueOf(cit.Marital)+"&P_MONTHLY_INCOME=0&P_MOTHER_NAME="+
	    					URLEncoder.encode(cit.MotherName,"utf-8")+"&P_BRANCH="+SplashScreenActivity.branch_code+
	    					"&P_NAT="+String.valueOf(1)+"&P_BENEF_FLAG=1&P_DRAWEE_FLAG=1&"+
	    					"P_PARTNER_NAME="+
	    					URLEncoder.encode(cit.SpouseName,"utf-8")+"&P_PARTNER_JOB="+
	    					URLEncoder.encode(cit.SpouseWork,"utf-8")+"&P_REG_1="+
	    					URLEncoder.encode(cit.RakemElSejel,"utf-8")+"&P_REG_PLACE="+
	    					regPlace+"&P_REG_2="+
	    					reg2+"&P_ha_tel2="+
	    					URLEncoder.encode(cit.Mobile,"utf-8")+"&P_ha_city_code="+
	    					cit.CityCode+"&P_ha_area_code="+
	    					cit.AreaCode+"&P_ha_suburb_code="+cit.SuburbCode+"&P_address="+
	    					URLEncoder.encode("","utf-8"); 
	    			
	    			encodedUrl = query;
	    					
	    					//"http://192.168.1.113/ibdaa/AddData.aspx?query=" 
	    					//+ URLEncoder.encode(un,"utf-8");
	    			
	    		} catch (UnsupportedEncodingException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	    		if(isOnline())
	         	 new CallWebRequestTask().execute(encodedUrl, cit.CITIZEN_ID);
	     	 }
	     	 
	     	 datasource.close();
	     	 
	        
	     	
	        this.stopSelf();
	    }
	    @Override
	    public void onDestroy() {

	        Log.d("ibdaaService", "Destroy");
	        // TODO Auto-generated method stub
	        super.onDestroy();
	    }
}
