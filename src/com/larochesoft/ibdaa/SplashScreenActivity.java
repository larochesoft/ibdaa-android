package com.larochesoft.ibdaa;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import android.provider.Settings;
import android.provider.Settings.Secure;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.NetworkOnMainThreadException;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;





public class SplashScreenActivity extends Activity {


	public static String username="NO USER!!!";
	public static String userid="0";
	public static String branch_code="0";
	public static String loan_supervisor="0";
	public static String area_supervisor="0";
	public static String baseUrl="http://192.168.1.113/ibdaa/";
	public static String baseAddress="192.168.1.113";
	
	
	public  boolean isOnline() {
	    ConnectivityManager cm =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	
	
	
    public static String connect(String url)
    {
    	try{

    	if (InetAddress.getByName(SplashScreenActivity.baseAddress).isReachable(10000))
    	{
        HttpClient httpclient = new DefaultHttpClient();
        String result = "";
        // Prepare a request object
        HttpGet httpget = new HttpGet(url); 

        // Execute the request
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);
            // Examine the response status
            Log.i("Praeda",response.getStatusLine().toString());

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();
            // If the response does not enclose an entity, there is no need
            // to worry about connection release

            if (entity != null) {

                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                 result= convertStreamToString(instream);
                // now you have the string representation of the HTML request
                instream.close();
            }


            
        } catch (Exception e) {
       	 
       	 return "error"+e.toString();
       	 
        }
        if (!result.contains("error"))
        {
            SplashScreenActivity.userid=result.split("_")[0].trim();
            SplashScreenActivity.username=result.split("_")[1].trim();
            SplashScreenActivity.branch_code=result.split("_")[2].trim();
            SplashScreenActivity.loan_supervisor=result.split("_")[3].trim();
            SplashScreenActivity.area_supervisor=result.split("_")[4].trim();
            

        }
        else {
        	
        }
        Log.d("ibdaa branch code", SplashScreenActivity.branch_code);
       
        
        return result;
    	}
    	}
    	catch(Exception ex)
    	{
    		
    	}
    	return "error: no connection";
    }

        private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
        
        private class CallWebRequestTask extends AsyncTask<Object, Object, Object> {
            protected String doInBackground(Object... urls) {
           	
            	
              	 
                return connect((String)urls[0]);
            }
            
            public CallWebRequestTask(){
                //citations = new ArrayList<String>();
            }

            protected void onPostExecute(Object result) {
           	Context context = getApplicationContext();
           	 CharSequence text = "Service Called!";
           	 int duration = Toast.LENGTH_LONG;
           	 if (result.toString().contains("error"))
           	 {
           	 Toast toast = Toast.makeText(context, 
           	        (String)result, duration);
           	 toast.show();
           	
     		
           
           	 }
           	 else
           		 
           	 {
           		 Log.d("ibdaa", (String)result);
           		
           		Toast toast = Toast.makeText(context, "Logging in as: "+SplashScreenActivity.username, duration);
              	 toast.show();
              	 
              	 
          		
              	 
           	 }
           	 
           	 Intent intent = new Intent(SplashScreenActivity.this,Ibdaa.class);
       		SplashScreenActivity.this.startActivity(intent);
            }

			
        }
        	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
       
        
        setContentView(R.layout.splash_screen);
        
        if (!isOnline())
        {
       	 Toast toast = Toast.makeText(getApplicationContext(), 
        	        "No internet connectivity, Please try again later!", Toast.LENGTH_LONG);
        	 toast.show();
        	 //finish();
        	 Intent intent = new Intent(SplashScreenActivity.this,Ibdaa.class);
        		SplashScreenActivity.this.startActivity(intent);
        }
        
        String deviceId = Settings.System.getString(getContentResolver(),Settings.System.ANDROID_ID);
        String encodedUrl=SplashScreenActivity.baseUrl+"AddData.aspx?STP=AUTH&DeviceId="+deviceId;
        
        new CallWebRequestTask().execute(encodedUrl);
        TextView txtSplash = (TextView)findViewById(R.id.textView1Login);
        txtSplash.setText("[Authenticating Device-"+deviceId+"]");
        
      
		
		
        
        /*
        citizenDataSource datasource = new citizenDataSource(this);
      	 datasource.open();
      	 
      	 Log.d("ibadaa", String.valueOf(datasource.getUnProcessedCount()));
      	 datasource.close();*/
        
    }
    
    
}