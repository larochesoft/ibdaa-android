package com.larochesoft.ibdaa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.TextView;



public class MySQLiteHelper extends SQLiteOpenHelper {
	  public static final String TABLE_CITIZEN = "CITIZEN";
	  public static final String COLUMN_ID = "CITIZEN_ID";
	  public static final String COLUMN_Name = "Name";
	  public static final String COLUMN_FatherName = "FatherName";
	  public static final String COLUMN_LastName = "LastName";
	  public static final String COLUMN_MotherName = "MotherName";
	  public static final String COLUMN_Gender = "Gender";
	  public static final String COLUMN_SpouseName = "SpouseName";
	  public static final String COLUMN_SpouseWork = "SpouseWork";
	  public static final String COLUMN_RakemElSejel = "RakemElSejel";
	  public static final String COLUMN_MahllatElSejel = "MahllatElSejel";
	  public static final String COLUMN_Mobile = "Mobile";
	  public static final String COLUMN_CurrentWork = "CurrentWork";
	  public static final String COLUMN_ProjectName = "ProjectName";
	  public static final String COLUMN_Marital = "Marital";
	  public static final String COLUMN_DOB = "DOB";
	  public static final String COLUMN_Status = "Status";
	  public static final String COLUMN_OraId = "OraId";
	  public static final String COLUMN_ProductId = "ProductId";
	  public static final String COLUMN_BranchCode = "BranchCode";
	  public static final String COLUMN_AreaSupervisor = "AreaSupervisor";
	  public static final String COLUMN_LoanSupervisor = "LoanSupervisor";
	  public static final String COLUMN_UserId = "UserId";
	  public static final String COLUMN_LoanAmount = "LoanAmount";
	  public static final String COLUMN_PotLoanAmount = "PotLoanAmount";
	  public static final String COLUMN_Freq = "Freq";
	  public static final String COLUMN_AppStatus = "AppStatus";
	  public static final String COLUMN_AreaCode = "AreaCode";
	  public static final String COLUMN_CityCode = "CityCode";
	  public static final String COLUMN_SuburbCode = "SuburbCode";
	  private static final String DATABASE_NAME = "ibdaa.db";
	  private static final int DATABASE_VERSION = 7;

	  
	  
	  // Database creation sql statement
	  private static final String DATABASE_CREATE = "create table "
	      + TABLE_CITIZEN + "(" + COLUMN_ID
	      + " integer primary key autoincrement, " + COLUMN_Name
	      + " text null, " + COLUMN_FatherName
	      + " text null, " + COLUMN_LastName
	      + " text  null, " + COLUMN_MotherName
	      + " text  null, " + COLUMN_Gender
	      + " integer  null, " + COLUMN_SpouseName
	      + " text  null, " + COLUMN_SpouseWork
	      + " text  null, " + COLUMN_RakemElSejel
	      + " text  null, " + COLUMN_MahllatElSejel
	      + " text  null, " + COLUMN_Mobile
	      + " text  null, " + COLUMN_CurrentWork
	      + " text  null, " + COLUMN_ProjectName
	      + " test  null, " + COLUMN_Marital
	      + " integer  null, " + COLUMN_DOB
	      + " text  null, " + COLUMN_Status
	      + " integer  null, " + COLUMN_OraId
	      + " integer  null, " + COLUMN_ProductId
	      + " integer  null, " + COLUMN_BranchCode
	      + " integer  null, " + COLUMN_AreaSupervisor
	      + " integer  null, " + COLUMN_LoanSupervisor
	      + " integer  null, " + COLUMN_UserId
	      + " integer  null, " + COLUMN_LoanAmount
	      + " real  null, " + COLUMN_PotLoanAmount
	      + " real  null, " + COLUMN_Freq
	      + " integer  null, " + COLUMN_AppStatus
	      + " integer  null, " + COLUMN_AreaCode
	      + " text  null, " + COLUMN_CityCode
	      + " text  null, " + COLUMN_SuburbCode
	      + " text  null);";

	  public MySQLiteHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    database.execSQL(DATABASE_CREATE);
	    
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(MySQLiteHelper.class.getName(),
	        "Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITIZEN);
	    onCreate(db);
	  }

}
