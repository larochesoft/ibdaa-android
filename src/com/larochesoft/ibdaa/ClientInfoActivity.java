package com.larochesoft.ibdaa;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import android.provider.Settings.Secure;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ClientInfoActivity extends Activity {

	 private TextView mDateDisplay;
	 private Menu m;
	 Spinner spinner;
	 private String maritalStatus="1";
	 private EditText myEdit;
	 private String maritalStatusName="";
	 public String branch="1";
	 public static String user="";
     private int mYear;
     private int mMonth;
     private int mDay;
     private long citizenId;
     static final int DATE_DIALOG_ID = 1;
     private RadioGroup radioMarital;
   
     private  DataBaseHelper myDbHelper;
     
     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
     	 MenuInflater inflater = getMenuInflater();
     	    inflater.inflate(R.menu.activity_ibdaa, menu);
     	   
     	   
     	    return true;
        
     }
    
     @Override
     public boolean onPrepareOptionsMenu(Menu menu) {
         MenuItem item= menu.findItem(R.id.menu_status);
         startService(new Intent(this, LoginSyncService.class));
         startService(new Intent(this, SyncService.class));
         startService(new Intent(this, AppSyncService.class));
         //depending on you conditions, either enable/disable
         citizenDataSource datasource = new citizenDataSource(this);
     	 datasource.open();
     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
     	 datasource.close();
     	 m=menu;
         return super.onPrepareOptionsMenu(menu);
     }

     public static String connect(String url)
     {
    	 
    	 try{

    	    	if (InetAddress.getByName(SplashScreenActivity.baseAddress).isReachable(3000))
    	    	{

         HttpClient httpclient = new DefaultHttpClient();
         String result = "";
         // Prepare a request object
         HttpGet httpget = new HttpGet(url); 

         // Execute the request
         HttpResponse response;
         try {
             response = httpclient.execute(httpget);
             // Examine the response status
             Log.i("Praeda",response.getStatusLine().toString());

             // Get hold of the response entity
             HttpEntity entity = response.getEntity();
             // If the response does not enclose an entity, there is no need
             // to worry about connection release

             if (entity != null) {

                 // A Simple JSON Response Read
                 InputStream instream = entity.getContent();
                  result= convertStreamToString(instream);
                 // now you have the string representation of the HTML request
                 instream.close();
             }


         } catch (Exception e) {
        	 
        	 return "error"+e.toString();
         }
         
         return result;
    	    	}
    	 }
    	    	catch(Exception ex)
    	    	{
    	    		
    	    	}
    	    	
    	    	return "error: connectivity";
     }

         private static String convertStreamToString(InputStream is) {
         /*
          * To convert the InputStream to String we use the BufferedReader.readLine()
          * method. We iterate until the BufferedReader return null which means
          * there's no more data to read. Each line will appended to a StringBuilder
          * and returned as String.
          */
         BufferedReader reader = new BufferedReader(new InputStreamReader(is));
         StringBuilder sb = new StringBuilder();

         String line = null;
         try {
             while ((line = reader.readLine()) != null) {
                 sb.append(line + "\n");
             }
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 is.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
         return sb.toString();
     }
         
         private class CallWebRequestTask extends AsyncTask<Object, Object, Object> {
             protected String doInBackground(Object... urls) {
                 return connect((String)urls[0]);
             }

             protected void onPostExecute(Object result) {
            	 Context context = getApplicationContext();
            	 
            	 int duration = Toast.LENGTH_LONG;
            	
            	 citizenDataSource datasource = new citizenDataSource(context);
            	 datasource.open();
            	 //result="LS_success_11212122";
            	 if (((String)result).contains("LS_error"))
            	 {
            		 datasource.updateStatus(citizenId, 1, 0);
            		 
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            	 }
            	 else if (((String)result).contains("LS_success"))
            	 {
            		 
            		 datasource.updateStatus(citizenId, 1, Long.parseLong(  ((String)result).trim().split("_")[2]));
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            		 
            		
            	 }
            	 else 
            	 {
            		 
            		 Toast toast = Toast.makeText(context, "Connectivity issue, saving locally!", duration);
                 	toast.show();
            	 }
            	
            	 datasource.close();
            	 
            	 Intent intent = new Intent(ClientInfoActivity.this, ApplicationActivity.class);
            	 Bundle b = new Bundle();
            	 b.putLong("CitizenId", citizenId); //Your id
            	 b.putString("Results",(String)result);
            	 intent.putExtras(b); 
                 startActivity(intent); 
                 finish();
             }

			
         }
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case android.R.id.home:
                 // app icon in action bar clicked; go home
                finish();
                 return true;
             case R.id.menu_status:
            	 DialogInterface.OnClickListener dialogClickListener1 = new DialogInterface.OnClickListener() {
         		    @Override
         		    public void onClick(DialogInterface dialog, int which) {
         		    	
         		    }
            	 };
            	 
            	 AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            	 
            	 citizenDataSource datasource = new citizenDataSource(this);
             	 datasource.open();
             	 String values="";
             	List<citizen> cList = datasource.getAllCitizens();
             	 for (int i=cList.size()-1; i>=0;i--)
             	 {
             		 citizen c=cList.get(i);
             		values+="Name:"+c.Name+" "+c.FatherName+" "+c.LastName +"\tLId:"+String.valueOf(c.CITIZEN_ID)
            				 +"\tRId:"+String.valueOf(c.OraId)+"\tStatus:"+String.valueOf(c.Status)+"\n";
             	 }
             	 datasource.close();
         		builder1.setMessage(values).setPositiveButton("Yes", dialogClickListener1)
         		    .setNegativeButton("No", dialogClickListener1).show();
            	
                 return true;
             case R.id.menu_next:
            	 DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            		    @Override
            		    public void onClick(DialogInterface dialog, int which) {
            		    	EditText txtName = (EditText) findViewById(R.id.txtName);
       		        	 TextView txtFathersName = (TextView) findViewById(R.id.txtFatherName);
       		        	 TextView txtLastName = (TextView) findViewById(R.id.txtLastName);
       		        	 TextView txtMotherName = (TextView) findViewById(R.id.txtMotherName);
       		        	 RadioButton chkGender =  (RadioButton) findViewById(R.id.chkGenderMale);
       		        	 TextView txtSpouseName = (TextView) findViewById(R.id.txtSpouseName);
       		        	 TextView txtSpouseWork = (TextView) findViewById(R.id.txtSpouseWork);
       		        	 TextView txtRakemElSejel = (TextView) findViewById(R.id.txtRakemElSejelValue);
       		        	 TextView txtMahllatElSejel = (TextView) findViewById(R.id.txtMahllatElSejelValue);
       		        	 TextView txtMobile = (TextView) findViewById(R.id.txtMobile);
       		        	 TextView txtAddress = (TextView) findViewById(R.id.txtAddress);
       		        	AutoCompleteTextView txtMahalla = (AutoCompleteTextView)
       		                 findViewById(R.id.txtMahalla);
       		        	
       		        	
       		        	
       		        	
       		        	
       		        	
       		        	String gender ="1";
       		        	String cityCode="B0";
		        		String areaCode="01";
		        		String suburbCode = "152";
		        		
            		        switch (which){
            		        case DialogInterface.BUTTON_POSITIVE:
            		        	
            		        	 
            		        	 
            		        	 
            		        	 
            		        	 
            		        	 if (!chkGender.isChecked()) gender="2";
            		        	 citizenDataSource datasource = new citizenDataSource(getApplicationContext());
            		        	 datasource.open();
            		        	 
            		        	 citizen c = null;
            		        	 c = datasource.createCitizen(txtName.getText().toString(), txtFathersName.getText().toString(), 
            		        			 txtLastName.getText().toString(), txtMotherName.getText().toString(), Integer.parseInt(gender), 
            		        			 txtSpouseName.getText().toString(), txtSpouseWork.getText().toString(), txtRakemElSejel.getText().toString(), 
            		        			 txtMahllatElSejel.getText().toString(), txtMobile.getText().toString(), "", 
            		        			 "", 1,String.valueOf(mYear)+
            		   					"-"+String.valueOf(mMonth+1)+"-"+String.valueOf(mDay)+"%2000:00:00", 0,0,0,0,0,0,0,0,0,0,areaCode,
            		   					cityCode, suburbCode);
            		        	 citizenId=c.CITIZEN_ID;
            		        	 //FIXME!!!
            		        	 
            		        	 datasource.close();
            		        	 
            		        	 
            		        	 myDbHelper = new DataBaseHelper(getApplicationContext());
            		             Cursor cur;
            		             
            		             try {

            		             	myDbHelper.createDataBase();
            		             	String qu="SELECT A.AREA_CODE, "
            		        				+" C.CITY_CODE, S.SUBURB_CODE FROM CODE_SUBURB S INNER JOIN CODE_AREA A " 
            		        				+ " ON A.AREA_CODE=S.AREA_CODE INNER JOIN CODE_CITY C ON C.CITY_CODE=S.CITY_CODE "
            		        				+ " WHERE S.SUBURB_ANAME || '-' || A.AREA_ADESC || '-' || C.CITY_ADESC = '"
            		        				+ txtMahalla.getText().toString()+"'";
            		        		cur = myDbHelper.getReadableDatabase().rawQuery(qu, null);
            		        		
            		        		cur.moveToFirst();
            		        		 if (cur.getCount()!=0)
            		        	     {
            		        			 areaCode = cur.getString(0);
            		        			 cityCode = cur.getString(1);
            		        			 suburbCode = cur.getString(2);
            		        	    	 	
            		        	     } 
            		        	     
            		        	     cur.close();

            		        	} catch (IOException ioe) {

            		        		throw new Error("Unable to create database");

            		        	}
            		   		String encodedUrl="";
            		   		try {
            		   			
            		   			String reg2="";
            		   			String regPlace="";
            		   			if (spinner.getSelectedItemPosition()==1)
            		   				reg2=URLEncoder.encode(txtMahllatElSejel.getText().toString(),"utf-8");
            		   			else regPlace=reg2=URLEncoder.encode(txtMahllatElSejel.getText().toString(),"utf-8");
            		   			String query=SplashScreenActivity.baseUrl+"AddData.aspx?STP=MOBILE_APP_CITIZEN&p_M_TYPE=1&P_name1="+
            		   					URLEncoder.encode(txtName.getText().toString(),"utf-8")+"&P_name2="+
            		   					URLEncoder.encode(txtFathersName.getText().toString(),"utf-8")+"&P_name4="+
            		   					URLEncoder.encode(txtLastName.getText().toString(),"utf-8")+"&P_BIRTH_DATE="+String.valueOf(mYear)+
            		   					"-"+String.valueOf(mMonth+1)+"-"+String.valueOf(mDay)+"%2000:00:00&"+
            		   					"P_GUAR_FLAG=2&P_GENDER="+gender+
            		   					"&P_MARITAL_STATUS="+maritalStatus+"&P_MONTHLY_INCOME=0&P_MOTHER_NAME="+
            		   					URLEncoder.encode(txtMotherName.getText().toString(),"utf-8")+"&P_BRANCH="+SplashScreenActivity.branch_code+
            		   					"&P_NAT="+String.valueOf(spinner.getSelectedItemPosition()+1)+"&P_BENEF_FLAG=1&P_DRAWEE_FLAG=1&"+
            		   					"P_PARTNER_NAME="+
            		   					URLEncoder.encode(txtSpouseName.getText().toString(),"utf-8")+"&P_PARTNER_JOB="+
            		   					URLEncoder.encode(txtSpouseWork.getText().toString(),"utf-8")+"&P_REG_1="+
            		   					URLEncoder.encode(txtRakemElSejel.getText().toString(),"utf-8")+"&P_REG_PLACE="+
            		   					regPlace+"&P_REG_2="+
            		   					reg2+"&P_ha_tel2="+
            		   					URLEncoder.encode(txtMobile.getText().toString(),"utf-8")+
            		   					"&P_ha_city_code="+cityCode+"&P_ha_area_code="+areaCode+
            		   					"&P_ha_suburb_code="+suburbCode+"&P_address="+
            		   					URLEncoder.encode(txtAddress.getText().toString(),"utf-8"); 
            		   			
            		   			encodedUrl = query;
            		   					
            		   					//"http://192.168.1.113/ibdaa/AddData.aspx?query=" 
            		   					//+ URLEncoder.encode(un,"utf-8");
            		   			
            		   		} catch (UnsupportedEncodingException e) {
            		   			// TODO Auto-generated catch block
            		   			e.printStackTrace();
            		   		}
            		   		
            		   			 new CallWebRequestTask().execute(encodedUrl);
            		   			 
            		        	 
            		                    // app icon in action bar clicked; go home
            		               	   
            		            break;

            		        case DialogInterface.BUTTON_NEGATIVE:
            		            //No button clicked
            		            break;
            		        }
            		    }
            		};

            		TextView txtName = (TextView) findViewById(R.id.txtName);
  		        	 TextView txtFathersName = (TextView) findViewById(R.id.txtFatherName);
  		        	 TextView txtLastName = (TextView) findViewById(R.id.txtLastName);
  		        	 TextView txtMotherName = (TextView) findViewById(R.id.txtMotherName);
  		        	 RadioButton chkGender =  (RadioButton) findViewById(R.id.chkGenderMale);
  		        	 TextView txtSpouseName = (TextView) findViewById(R.id.txtSpouseName);
  		        	 TextView txtSpouseWork = (TextView) findViewById(R.id.txtSpouseWork);
  		        	 TextView txtRakemElSejelValue = (TextView) findViewById(R.id.txtRakemElSejelValue);
  		        	 TextView txtMahllatElSejelValue = (TextView) findViewById(R.id.txtMahllatElSejelValue);

  		        	 TextView txtRakemElSejel = (TextView) findViewById(R.id.txtRakemElSejel);
  		        	 TextView txtMahllatElSejel = (TextView) findViewById(R.id.txtMahllatElSejel);
  		        	 TextView txtMobile = (TextView) findViewById(R.id.txtMobile);
  		        	 TextView txtAddress = (TextView) findViewById(R.id.txtAddress);
  		        	AutoCompleteTextView txtMahalla = (AutoCompleteTextView)
      		                 findViewById(R.id.txtMahalla);
  		        	 String gender="1";
  		        	 boolean inValid=false;
  		        	if( txtName.getText().toString().length() == 0 )
   		        	{
   		        		txtName.setError( "Required!" );
   		        		inValid=true;
   		        	}
  		        	else txtName.setError( null);
  		        	
  		        	
  		        	if( txtFathersName.getText().toString().length() == 0 )
   		        	{
  		        		txtFathersName.setError( "Required!" );
  		        		inValid=true;
   		        	} else txtFathersName.setError( null);
  		        	
  		        	
  		        	if( txtLastName.getText().toString().length() == 0 )
   		        	{
  		        		txtLastName.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtLastName.setError( null);
  		        	
  		        	
  		        	if( txtMotherName.getText().toString().length() == 0 )
   		        	{
  		        		txtMotherName.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtMotherName.setError( null);
  		        	
  		        	
  		        	if( txtRakemElSejelValue.getText().toString().length() == 0 )
   		        	{
  		        		txtRakemElSejelValue.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtRakemElSejelValue.setError( null);
  		        	
  		        	
  		        	if( txtMahllatElSejelValue.getText().toString().length() == 0 )
   		        	{
  		        		txtMahllatElSejelValue.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtMahllatElSejelValue.setError( null);
  		        	
  		        	
  		        	if( txtMobile.getText().toString().length() == 0 )
   		        	{
  		        		txtMobile.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtMobile.setError( null);
  		        	
  		        	
  		        	if( txtAddress.getText().toString().length() == 0 )
   		        	{
  		        		txtAddress.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtAddress.setError( null);
  		        	
  		        	
  		        	if( txtMahalla.getText().toString().length() == 0 )
   		        	{
  		        		txtMahalla.setError( "Required!" );
  		        		inValid= true;
   		        	}else txtMahalla.setError( null);
  		        	
  		        	
  		        	if (maritalStatus.compareTo("2")==0){
  		        		if( txtSpouseName.getText().toString().length() == 0 )
  	   		        	{
  		        			txtSpouseName.setError( "Required!" );
  		        			inValid= true;
  	   		        	}else txtSpouseName.setError( null);
  		        	}  else txtSpouseName.setError( null);
  		        	
  		        	
  		        	
  		        	if (inValid) return true;
  		        	String[] nats = getResources().getStringArray(R.array.ins_array);

  		        	if (!chkGender.isChecked()) gender="2";
            		String alertValues = "الاسم:"+txtName.getText().toString()+" "+txtFathersName.getText().toString() 
            				+" " + txtLastName.getText().toString()
            				+"\nاسم الام:"+ txtMotherName.getText().toString()
            				+"\nالجنس:"+((gender=="1")?"ذكر":"انثئ")
            				+"\nالحاله الاجتماعيه:"+maritalStatusName;
            		
            		if (maritalStatus.compareTo("2")==0){
            			alertValues+="\nالزوج - ه:"+txtSpouseName.getText().toString()
            				+ "\nعمل الزوج - ه: "+txtSpouseWork.getText().toString();
            		}
            		alertValues+="\nالجنسيه:"+nats[spinner.getSelectedItemPosition()]
            				+"\n"+txtRakemElSejel.getText().toString()+":"+txtRakemElSejelValue.getText().toString()
            				+"\n"+txtMahllatElSejel.getText().toString()+":"+txtMahllatElSejelValue.getText().toString()
            				+"\nالموبايل:"+txtMobile.getText().toString()
            				+"\nمحله السكن:"+txtMahalla.getText().toString()
            				+"\nالعنوان:"+txtAddress.getText().toString()
            				+"\nتاريخ الميلاد:"+String.valueOf(mYear)+"-"+String.valueOf(mMonth+1)+"-"+String.valueOf(mDay);
            		AlertDialog.Builder builder = new AlertDialog.Builder(this);
            		builder.setMessage("هل انت متاكد-ه؟\n"+alertValues).setPositiveButton("Yes", dialogClickListener)
            		    .setNegativeButton("No", dialogClickListener).show();
     	 
                 return true;
             default:
                 return super.onOptionsItemSelected(item);
         }
     }
     
     
     
     
     public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        
         setContentView(R.layout.client_info_activity);
         
         startService(new Intent(this, LoginSyncService.class));
         startService(new Intent(this, SyncService.class));
         startService(new Intent(this, AppSyncService.class));
         
         myEdit = (EditText) findViewById(R.id.txtMobile);
         myEdit.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(!hasFocus)
				{
					if (myEdit.getText().length() < 8) {
						Log.d("ibdaa nuuu", "here");
                        Toast.makeText(ClientInfoActivity.this, "Not enough characters", Toast.LENGTH_LONG);
                        
                    } 
				}
				
			}
           
         });
         
         Handler handler = new Handler();
         
         // run a thread after 2 seconds to start the home screen
         handler.postDelayed(new Runnable() {
  
             @Override
             public void run() {
  
                 // make sure we close the splash screen so the user won't come back when it presses back key
  
                
                if (m!=null)
                {
             	   MenuItem item= m.findItem(R.id.menu_status);
       	         //depending on you conditions, either enable/disable
       	         citizenDataSource datasource = new citizenDataSource(getApplicationContext());
       	     	 datasource.open();
       	     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
       	     	 datasource.close();
                }
             }
  
         }, 10000); // time in milliseconds (1 second = 1000 milliseconds) until the run() method will be called
  
         
         
         ActionBar actionBar = getActionBar();
         actionBar.setDisplayHomeAsUpEnabled(true);
     
         
         
         
     mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
     Button pickDate = (Button) findViewById(R.id.pickDate);
     pickDate.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                     showDialog(DATE_DIALOG_ID);
             }
     });
     
     
    
     mDateDisplay.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                     showDialog(DATE_DIALOG_ID);
             }
     });
     
     
     final Calendar c = Calendar.getInstance();
     mYear = c.get(Calendar.YEAR)-18;
     mMonth = c.get(Calendar.MONTH);
     mDay = c.get(Calendar.DAY_OF_MONTH);
     updateDisplay();
     
     
     
     radioMarital = (RadioGroup) findViewById(R.id.radioMarital);
     radioMarital.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

         public void onCheckedChanged(RadioGroup group,
                 int checkedId) {
        	 RadioButton  rb=(RadioButton)findViewById(checkedId);

        	 maritalStatus = rb.getTag().toString();
        	 maritalStatusName = rb.getText().toString();
            if (rb.getTag().toString().equals("2") || rb.getTag().toString().equals("4"))
            {
            	
            	LinearLayout llHusbandName = (LinearLayout) findViewById(R.id.llHusbandName);
            	llHusbandName.setVisibility(View.VISIBLE);
            	LinearLayout llHusbandWork = (LinearLayout) findViewById(R.id.llHusbandWork);
            	llHusbandWork.setVisibility(View.VISIBLE);
            }
            else
            {
            	LinearLayout llHusbandName = (LinearLayout) findViewById(R.id.llHusbandName);
            	llHusbandName.setVisibility(View.GONE);
            	LinearLayout llHusbandWork = (LinearLayout) findViewById(R.id.llHusbandWork);
            	llHusbandWork.setVisibility(View.GONE);
            }

         }
     });
     
     spinner = (Spinner) findViewById(R.id.spnNationality);
     // Create an ArrayAdapter using the string array and a default spinner layout
     ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
             R.array.ins_array, android.R.layout.simple_spinner_item);
     // Specify the layout to use when the list of choices appears
     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
     // Apply the adapter to the spinner
     spinner.setAdapter(adapter);
     
     
     spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
    	   


			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				
				TextView txtRakemElSejel = (TextView)findViewById(R.id.txtRakemElSejel);
			    TextView txtMahllatElSejel = (TextView)findViewById(R.id.txtMahllatElSejel);
			     
				if (arg2==1)
				{
					
					txtRakemElSejel.setText("رقم الملف");
					txtMahllatElSejel.setText("رقم البيان الاحصائي");
				}
				else
				{
					txtRakemElSejel.setText("رقم السجل");
					txtMahllatElSejel.setText("محله السجل");
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}

    	});
     
     myDbHelper = new DataBaseHelper(this);
     Cursor cur;
     
     ArrayAdapter<String> addressAdapter = new ArrayAdapter<String>(this,
             android.R.layout.simple_dropdown_item_1line);
     try {

     	myDbHelper.createDataBase();
		cur = myDbHelper.getReadableDatabase().rawQuery("SELECT A.AREA_ADESC, C.CITY_ADESC, S.SUBURB_ANAME FROM CODE_SUBURB S INNER JOIN CODE_AREA A ON A.AREA_CODE=S.AREA_CODE INNER JOIN CODE_CITY C ON C.CITY_CODE=S.CITY_CODE", null);
			
		cur.moveToFirst();
		int num=0;
		 do
	     {
	    	 	addressAdapter.add(cur.getString(2)+"-"+cur.getString(0)+"-"+cur.getString(1));
	    	 	
	     } while (cur.moveToNext());
	     
		
	     cur.close();

	} catch (IOException ioe) {

		throw new Error("Unable to create database");

	}

	try {

		

	}catch(SQLException sqle){

		throw sqle;

	}
    
    // addressAdapter.add(cur.getString(0));
     
    
     
     final AutoCompleteTextView textView = (AutoCompleteTextView)
             findViewById(R.id.txtMahalla);
     textView.setAdapter(addressAdapter);
     
     textView.addTextChangedListener(new TextWatcher() {
    	    public void onTextChanged(CharSequence s, int start, int before, int count) {
                
                
                
            }
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
                // do nothing
            }
            
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
				
				
				Editable currentText = textView.getText();
				//TextView txtQadaa = (TextView)findViewById(R.id.txtQadaa);
			    //TextView txtMohafaza = (TextView)findViewById(R.id.txtMohafaza);
			    String [] args={""};
			    if (currentText.toString().split("-").length>1){
			    	args[0]=currentText.toString().split("-")[0];
			    }
			    else 
			    	args[0] =currentText.toString();
				Cursor cur = myDbHelper.getReadableDatabase().rawQuery("SELECT A.AREA_ADESC, C.CITY_ADESC FROM CODE_SUBURB S INNER JOIN CODE_AREA A ON A.AREA_CODE=S.AREA_CODE INNER JOIN CODE_CITY C ON C.CITY_CODE=S.CITY_CODE WHERE SUBURB_ANAME=?", args);
				
				if (cur.moveToFirst()){
				//txtQadaa.setText(cur.getString(0));
				//txtMohafaza.setText(cur.getString(1));
				}
			     cur.close();
			     
			     
			     
			    
			    
			}
        });
     
     }
     
     
     @Override
     protected Dialog onCreateDialog(int id) {
             switch (id) {
             case DATE_DIALOG_ID:
                     return new DatePickerDialog(this,
                             mDateSetListener,
                             mYear, mMonth, mDay);
             }
             return null;
     }
     protected void onPrepareDialog(int id, Dialog dialog) {
             switch (id) {
             case DATE_DIALOG_ID:
                     ((DatePickerDialog) dialog).updateDate(mYear, mMonth, mDay);
                     break;
                   
                     
             }
     }    
     private void updateDisplay() {
    	 Calendar c = Calendar.getInstance(); 
    	 int year = c.get(Calendar.YEAR);
    	 
    	 if ((year-mYear)<18 || (year-mYear)>62)
    	 {
    		 mDateDisplay.setError("Between 18 and 62!");
    	 }
    	 else {
    		 mDateDisplay.setError(null);
    		 mDateDisplay.setText(
    	 
                 new StringBuilder()
                 // Month is 0 based so add 1
                 .append(mMonth + 1).append("-")
                 .append(mDay).append("-")
                 .append(mYear).append(" "));
    	 }
     }
     private DatePickerDialog.OnDateSetListener mDateSetListener =
             new DatePickerDialog.OnDateSetListener() {
             public void onDateSet(DatePicker view, int year, int monthOfYear,
                             int dayOfMonth) {
                     mYear = year;
                     mMonth = monthOfYear;
                     mDay = dayOfMonth;
                     updateDisplay();
             }
     };
}
