package com.larochesoft.ibdaa;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class AppSyncService extends Service {
	 @Override
	    public IBinder onBind(Intent arg0) {
	        // TODO Auto-generated method stub
	        return null;
	    }
	 
	 public  boolean isOnline() {
		    ConnectivityManager cm =
		        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    return false;
		}
	 
	 public static String connect(String url, String CitizenId)
     {

		 try{

 	    	if (InetAddress.getByName(SplashScreenActivity.baseAddress).isReachable(10000))
 	    	{
         HttpClient httpclient = new DefaultHttpClient();
         String result = "";
         // Prepare a request object
         HttpGet httpget = new HttpGet(url); 

         // Execute the request
         HttpResponse response;
         try {
             response = httpclient.execute(httpget);
             // Examine the response status
             Log.i("Praeda",response.getStatusLine().toString());

             // Get hold of the response entity
             HttpEntity entity = response.getEntity();
             // If the response does not enclose an entity, there is no need
             // to worry about connection release

             if (entity != null) {

                 // A Simple JSON Response Read
                 InputStream instream = entity.getContent();
                  result= convertStreamToString(instream);
                 // now you have the string representation of the HTML request
                 instream.close();
             }


         } catch (Exception e) {
        	 
        	 return "error"+e.toString();
         }
         return result.trim() + "&"+CitizenId.trim();
 	    	 }
		 }
		    	catch(Exception ex)
		    	{
		    		
		    	}
		    	
		    	return "error: connectivity";
     }

         private static String convertStreamToString(InputStream is) {
         /*
          * To convert the InputStream to String we use the BufferedReader.readLine()
          * method. We iterate until the BufferedReader return null which means
          * there's no more data to read. Each line will appended to a StringBuilder
          * and returned as String.
          */
         BufferedReader reader = new BufferedReader(new InputStreamReader(is));
         StringBuilder sb = new StringBuilder();

         String line = null;
         try {
             while ((line = reader.readLine()) != null) {
                 sb.append(line + "\n");
             }
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 is.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
         return sb.toString();
     }
         
         private class CallWebRequestTask extends AsyncTask<Object, Object, Object> {
             protected String doInBackground(Object... urls) {
            	
                 return connect((String)urls[0],urls[1].toString());
             }

             protected void onPostExecute(Object result1) {
            	 Context context = getApplicationContext();
            	 int duration = Toast.LENGTH_LONG;
            	 
            	String result = ((String)result1).split("&")[0];
            	 citizenDataSource datasource = new citizenDataSource(context);
            	 datasource.open();
            	 if (((String)result).contains("LS_error"))
            	 {
            		 long citizenId = Long.parseLong(((String)result1).split("&")[1]);
            		 
            		 datasource.updateAppStatus(citizenId, 1);
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            	 }
            	 else if (((String)result).contains("LS_success"))
            	 {
            		 long citizenId = Long.parseLong(((String)result1).split("&")[1]);
            		 
            		 datasource.updateAppStatus(citizenId, 1);
            		Toast toast = Toast.makeText(context, (String) result, duration);
                	toast.show();
            	 }
            	 else 
            	 {
            		
            		//Toast toast = Toast.makeText(context, "Connectivity issue, saving locally!", duration);
                 	//toast.show();
            	 }
            	
            	 datasource.close();
            	 
             }

			
         }
         
	  @Override
	    public void onStart(Intent intent, int startId) {
	        // TODO Auto-generated method stub
	        super.onStart(intent, startId);
	        
	        citizenDataSource datasource = new citizenDataSource(this);
	     	 datasource.open();
	     	 List <citizen> pendingApps=datasource.getAllPendingApps();
	     	 for (int i=0; i<pendingApps.size(); i++)
	     	 {
	     		citizen cit = pendingApps.get(i);
	     		 String P_LOAN_TYPE="";
             	 String P_PRODUCT="";
             	 String P_PRODUCT_TYPE="";
             	 
             	 
             	 switch(cit.ProductId)
             	 {
             	 case 0:
             		 P_LOAN_TYPE="2";
             		 P_PRODUCT="3";
             		 P_PRODUCT_TYPE="3";
             		 break;
             	 case 1:
             		P_LOAN_TYPE="1";
            		 P_PRODUCT="1";
            		 P_PRODUCT_TYPE="1";
            		 
             		 break;
             	 case 2:
             		P_LOAN_TYPE="2";
            		 P_PRODUCT="2";
            		 P_PRODUCT_TYPE="2";
            		 
             		 break;
             	 case 3:
             		P_LOAN_TYPE="2";
            		 P_PRODUCT="3";
            		 P_PRODUCT_TYPE="4";
            		 
             		 break;
             	 default: 
             		 break;
             	 }
	     		String encodedUrl="";
	     		if(cit.OraId!=0)
	     		{
	    		try {
	    			
	    			
	    			encodedUrl = SplashScreenActivity.baseUrl+"AddData.aspx?STP=MOBILE_APP_APPLICATION&p_M_TYPE=1&p_branch_code="+
	 						SplashScreenActivity.branch_code+"&p_applicant="+String.valueOf(cit.OraId)+"&P_LOAN_TYPE=" +P_LOAN_TYPE+"&P_freq="
	 						+String.valueOf(cit.Freq)+"&P_PRODUCT="+P_PRODUCT+"&P_KNOWING_CODE="+"1"+"&P_REASON_CODE="+"1"
	 						+"&P_PRODUCT_TYPE="+P_PRODUCT_TYPE+"&P_LOAN_REASON_USE=-"+""+"&P_WORK_DESC="
	 						+URLEncoder.encode(cit.CurrentWork,"utf-8")+"&P_PROJECT_DESC="
	 						+URLEncoder.encode(cit.ProjectName,"utf-8")+ "&P_AREA_SUPERVISOR="
	 						+SplashScreenActivity.area_supervisor+"&P_LOAN_OFFICER="+SplashScreenActivity.userid
	 						+"&P_LOAN_SUPERVISOR="+SplashScreenActivity.loan_supervisor+"&P_AMOUNT="
	 						+URLEncoder.encode(String.valueOf(cit.LoanAmount),"utf-8")+"&P_POT_AMOUNT="
	 		 						+URLEncoder.encode(String.valueOf(cit.PotLoanAmount),"utf-8");
	    			
	    			
	    			
	    		} catch (UnsupportedEncodingException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	    		if (isOnline())
	         	 new CallWebRequestTask().execute(encodedUrl, cit.CITIZEN_ID);
	     		}
	     	 }
	     	 
	     	 datasource.close();
	     	 
	     	 
	        
	        this.stopSelf();
	    }
	    @Override
	    public void onDestroy() {

	        Log.d("ibdaaService", "Destroy");
	        // TODO Auto-generated method stub
	        super.onDestroy();
	    }
}
