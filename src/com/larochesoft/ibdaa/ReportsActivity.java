package com.larochesoft.ibdaa;

import java.util.Calendar;
import java.util.Date;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ReportsActivity extends Activity {

	private EditText txtFromDate;
	private Button button;
	private Button button2;
	private Button button3;
	private Button button4;
	private EditText txtToDate;
	 private TextView mDateDisplayFrom;
	 private TextView mDateDisplayTo;
	 private int mYearF;
     private int mMonthF;
     private int mDayF;
     static final int DATE_DIALOG_ID = 1;
     private int mYearT;
     private int mMonthT;
     private int mDayT;
     static final int DATE_DIALOG_ID_To = 2;
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	 MenuInflater inflater = getMenuInflater();
    	    inflater.inflate(R.menu.reports_menu, menu);
    	   
    	   
    	    return true;
       
    }
	
	 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        
            case R.id.menu_LoansByOfficer:
                // app icon in action bar clicked; go home
               button.setVisibility(View.VISIBLE);
               button2.setVisibility(View.GONE);
               button3.setVisibility(View.GONE);
                return true;
            case R.id.menu_GroupedLoansByOfficer:
                // app icon in action bar clicked; go home
               button.setVisibility(View.GONE);
               button2.setVisibility(View.VISIBLE);
               button3.setVisibility(View.GONE);
                return true;
            case R.id.menu_ActiveLoansByOfficer:
                // app icon in action bar clicked; go home
               button.setVisibility(View.GONE);
               button2.setVisibility(View.GONE);
               button3.setVisibility(View.VISIBLE	);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	

    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_activity);
        
		final Context context = this;

        
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        
        mDateDisplayFrom = (TextView) findViewById(R.id.dateDisplayFrom);
        Button pickDate1 = (Button) findViewById(R.id.pickDate1);
        pickDate1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                        showDialog(DATE_DIALOG_ID);
                }
        });
        final Calendar c = Calendar.getInstance();
        
        mYearF = c.get(Calendar.YEAR);
        mMonthF = c.get(Calendar.MONTH);
        mDayF = c.get(Calendar.DAY_OF_MONTH);
        updateDisplayFrom();
        
       
        mDateDisplayTo = (TextView) findViewById(R.id.dateDisplayTo);
        Button pickDate2 = (Button) findViewById(R.id.pickDate2);
        pickDate2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                        showDialog(DATE_DIALOG_ID_To);
                }
        });
        final Calendar c2 = Calendar.getInstance();
        Date d = new Date();
        d.setMonth(d.getMonth()+1);
        c2.setTime(d);
        mYearT = c2.get(Calendar.YEAR);
        mMonthT = c2.get(Calendar.MONTH);
        mDayT = c2.get(Calendar.DAY_OF_MONTH);
        updateDisplayTo();
        
         button = (Button) findViewById(R.id.btnReport1);
             

		button.setOnClickListener(new OnClickListener() {
 
		  @Override
		  public void onClick(View arg0) {
			ReportsWebViewActivity.URL=SplashScreenActivity.baseUrl+"rptLoansByOfficer.aspx?Officer="+
		  SplashScreenActivity.userid+"&fromDate="+mDateDisplayFrom.getText()+"&toDate="+mDateDisplayTo.getText();
			Log.d("ibdaa", ReportsWebViewActivity.URL);
		    Intent intent = new Intent(context, ReportsWebViewActivity.class);
		    startActivity(intent);
		  }
 
		});
		
		
		    button2 = (Button) findViewById(R.id.btnReport2);
           
			
			button2.setOnClickListener(new OnClickListener() {
	 
			  @Override
			  public void onClick(View arg0) {
				ReportsWebViewActivity.URL=SplashScreenActivity.baseUrl+"rptLoansByOfficeGrouped.aspx?Officer="+
			  SplashScreenActivity.userid+"&fromDate="+mDateDisplayFrom.getText()+"&toDate="+mDateDisplayTo.getText();
				Log.d("ibdaa", ReportsWebViewActivity.URL);
			    Intent intent = new Intent(context, ReportsWebViewActivity.class);
			    startActivity(intent);
			  }
	 
			});
		
			  button3 = (Button) findViewById(R.id.btnReport3);
	           
				
				button3.setOnClickListener(new OnClickListener() {
		 
				  @Override
				  public void onClick(View arg0) {
					ReportsWebViewActivity.URL=SplashScreenActivity.baseUrl+"rptActiveLoansByOfficer.aspx?Officer="+
				  SplashScreenActivity.userid+"&fromDate="+mDateDisplayFrom.getText()+"&toDate="+mDateDisplayTo.getText();
					Log.d("ibdaa", ReportsWebViewActivity.URL);
				    Intent intent = new Intent(context, ReportsWebViewActivity.class);
				    startActivity(intent);
				  }
		 
				});
				
				  button4 = (Button) findViewById(R.id.btnReport4);
		           
					
					button4.setOnClickListener(new OnClickListener() {
			 
					  @Override
					  public void onClick(View arg0) {
						ReportsWebViewActivity.URL=SplashScreenActivity.baseUrl+"rptRemIstallmentReport.aspx?Officer="+
					  SplashScreenActivity.userid+"&fromDate="+mDateDisplayFrom.getText()+"&toDate="+mDateDisplayTo.getText();
						Log.d("ibdaa", ReportsWebViewActivity.URL);
					    Intent intent = new Intent(context, ReportsWebViewActivity.class);
					    startActivity(intent);
					  }
			 
					});
			
        /*Spinner spinner = (Spinner) findViewById(R.id.rep_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.rep_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);*/
        
    }
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
            switch (id) {
            case DATE_DIALOG_ID:
                    return new DatePickerDialog(this,
                            mDateSetListenerFrom,
                            mYearF, mMonthF, mDayF);
            case DATE_DIALOG_ID_To:
                return new DatePickerDialog(this,
                        mDateSetListenerTo,
                        mYearT, mMonthT, mDayT);
            }
            return null;
    }
    protected void onPrepareDialog(int id, Dialog dialog) {
            switch (id) {
            case DATE_DIALOG_ID:
                    ((DatePickerDialog) dialog).updateDate(mYearF, mMonthF, mDayF);
                    break;
            case DATE_DIALOG_ID_To:
                ((DatePickerDialog) dialog).updateDate(mYearT, mMonthT, mDayT);
                break;
            }
    }    
    private void updateDisplayFrom() {
            mDateDisplayFrom.setText(
                    new StringBuilder()
                    // Month is 0 based so add 1
                    .append(mMonthF + 1).append("/")
                    .append(mDayF).append("/")
                    .append(mYearF).append(" "));
    }
    private DatePickerDialog.OnDateSetListener mDateSetListenerFrom =
            new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                            int dayOfMonth) {
                    mYearF = year;
                    mMonthF = monthOfYear;
                    mDayF = dayOfMonth;
                    updateDisplayFrom();
            }
    };
    private void updateDisplayTo() {
        mDateDisplayTo.setText(
                new StringBuilder()
                // Month is 0 based so add 1
                .append(mMonthT + 1).append("/")
                .append(mDayT).append("/")
                .append(mYearT).append(" "));
}
    private DatePickerDialog.OnDateSetListener mDateSetListenerTo =
            new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                            int dayOfMonth) {
                    mYearT = year;
                    mMonthT = monthOfYear;
                    mDayT = dayOfMonth;
                    updateDisplayTo();
            }
    };
}
