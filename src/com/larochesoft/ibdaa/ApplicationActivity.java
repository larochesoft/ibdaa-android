package com.larochesoft.ibdaa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ApplicationActivity extends Activity {

	
	private Menu m;
	 @Override
     public boolean onCreateOptionsMenu(Menu menu) {
     	 MenuInflater inflater = getMenuInflater();
     	    inflater.inflate(R.menu.menu_app, menu);
     	    return true;
        
     }
	 
	 @Override
     public boolean onPrepareOptionsMenu(Menu menu) {
         MenuItem item= menu.findItem(R.id.menu_status);
         startService(new Intent(this, LoginSyncService.class));
         startService(new Intent(this, SyncService.class));
         startService(new Intent(this, AppSyncService.class));
         //depending on you conditions, either enable/disable
         citizenDataSource datasource = new citizenDataSource(this);
     	 datasource.open();
     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
     	 datasource.close();
     	 m=menu;
         return super.onPrepareOptionsMenu(menu);
     }
	 public static String connect(String url)
     {
		 try{

 	    	if (InetAddress.getByName(SplashScreenActivity.baseAddress).isReachable(3000))
 	    	{

         HttpClient httpclient = new DefaultHttpClient();
         String result = "";
         // Prepare a request object
         HttpGet httpget = new HttpGet(url); 

         // Execute the request
         HttpResponse response;
         try {
             response = httpclient.execute(httpget);
             // Examine the response status
             Log.i("Praeda",response.getStatusLine().toString());

             // Get hold of the response entity
             HttpEntity entity = response.getEntity();
             // If the response does not enclose an entity, there is no need
             // to worry about connection release

             if (entity != null) {

                 // A Simple JSON Response Read
                 InputStream instream = entity.getContent();
                  result= convertStreamToString(instream);
                 // now you have the string representation of the HTML request
                 instream.close();
             }


         } catch (Exception e) {
        	 
        	 return "error"+e.toString();
         }
         Log.d("ibdaa", result);
         return result;
         
     }
}
   	catch(Exception ex)
   	{
   		
   	}
   	
   	return "error: connectivity";
     }

         private static String convertStreamToString(InputStream is) {
         /*
          * To convert the InputStream to String we use the BufferedReader.readLine()
          * method. We iterate until the BufferedReader return null which means
          * there's no more data to read. Each line will appended to a StringBuilder
          * and returned as String.
          */
         BufferedReader reader = new BufferedReader(new InputStreamReader(is));
         StringBuilder sb = new StringBuilder();

         String line = null;
         try {
             while ((line = reader.readLine()) != null) {
                 sb.append(line + "\n");
             }
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             try {
                 is.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
         return sb.toString();
     }
         
         private class CallWebRequestTask extends AsyncTask {
             protected String doInBackground(Object... urls) {
                 return connect((String)urls[0]);
             }

             protected void onPostExecute(Object result) {
            	 Context context = getApplicationContext();
            	 int duration = Toast.LENGTH_LONG;
            	 Toast toast = Toast.makeText(context, (String)result, duration);
            	 toast.show();
            	 finish();
             }

			
         }
     
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case android.R.id.home:
                 // app icon in action bar clicked; go home
                finish();
                 return true;
             case R.id.menu_save:
            	 
             	/* new Thread(new Runnable() {
             		    public void run() {
             		    	 connect("http://www.google.com");
                        	            		    }
             		  }).start();
             	
             	 */
            	 
            	 Bundle b = getIntent().getExtras();
            	 long citizenId = b.getLong("CitizenId");
            	 citizenDataSource datasource = new citizenDataSource(getApplicationContext());
            	 datasource.open();
            	 citizen c = datasource.getCitizenById(citizenId);
            	 datasource.close();
            	 
            	 TextView txtCurrentWork = (TextView) findViewById(R.id.txtCurrentWork);
            	 TextView txtProjectName = (TextView) findViewById(R.id.txtProjectName);
             	 TextView txtPaymentNum = (TextView) findViewById(R.id.txtPaymentNum);
             	 TextView txtPaymentAmt = (TextView) findViewById(R.id.txtPaymentAmt);
             	 TextView txtLoanAmount = (TextView) findViewById(R.id.txtLoanAmount);
             	 Spinner spnPackage = (Spinner) findViewById(R.id.package_spinner);
             	 
             	 String P_LOAN_TYPE="";
             	 String P_PRODUCT="";
             	 String P_PRODUCT_TYPE="";
             	 
             	 
             	 switch(spnPackage.getSelectedItemPosition())
             	 {
             	 
             	 case 0:
             		P_LOAN_TYPE="1";
            		 P_PRODUCT="1";
            		 P_PRODUCT_TYPE="1";
             		 break;
             	 case 1:
             		P_LOAN_TYPE="2";
            		 P_PRODUCT="2";
            		 P_PRODUCT_TYPE="2";
             		 break;
             	 case 2:
             		P_LOAN_TYPE="2";
            		 P_PRODUCT="3";
            		 P_PRODUCT_TYPE="3";
             		 break;
             	case 3:
            		 P_LOAN_TYPE="2";
            		 P_PRODUCT="4";
            		 P_PRODUCT_TYPE="3";
            		 break;
             	 default: 
             		 break;
             	 }
             	 
             	 
             	 
             	 
             	 
             	 boolean inValid=false;
		        	/*if( txtCurrentWork.getText().toString().length() == 0 )
		        	{
		        		txtCurrentWork.setError( "Required!" );
		        		inValid=true;
		        	}
		        	else txtCurrentWork.setError( null);
		        	
		        	if( txtProjectName.getText().toString().length() == 0 )
		        	{
		        		txtProjectName.setError( "Required!" );
		        		inValid=true;
		        	}
		        	else txtProjectName.setError( null);
		        	*/
		        	
		        	if( txtPaymentNum.getText().toString().length() == 0 )
		        	{
		        		txtPaymentNum.setError( "Required!" );
		        		inValid=true;
		        	}
		        	else txtPaymentNum.setError( null);
		        	
		        	if( txtLoanAmount.getText().toString().length() == 0 )
		        	{
		        		txtLoanAmount.setError( "Required!" );
		        		inValid=true;
		        	}
		        	else txtLoanAmount.setError( null);
		        	
		        	if (inValid) return true; 
		        	switch(spnPackage.getSelectedItemPosition())
	             	 {
	             	 case 0:
	             		if( Integer.parseInt( txtPaymentNum.getText().toString()) >12 || 
	             				Integer.parseInt( txtPaymentNum.getText().toString())<6)
			        	{
			        		txtPaymentNum.setError( "Between 6 & 12" );
			        		inValid=true;
			        	}
			        	else txtPaymentNum.setError( null);
			        	
			        	if( Float.parseFloat( txtLoanAmount.getText().toString())>1500000 ||
			        			Float.parseFloat( txtLoanAmount.getText().toString())<150000)
			        	{
			        		txtLoanAmount.setError( "Between 150,000 & 1,500,000 LL" );
			        		inValid=true;
			        	}
			        	else txtLoanAmount.setError( null);
			        	
			        	if (c.Gender!=2) {
			        		Context context = getApplicationContext();
			            	
			            	 int duration = Toast.LENGTH_LONG;
			            	 Toast toast = Toast.makeText(context, "قرض سيدتي للاناث فقط!", duration);
			            	 toast.show();
			            	 inValid=true;
			        	}
	             		 break;
	             	 case 1:
	             		if( Integer.parseInt( txtPaymentNum.getText().toString()) >48 || 
	             				Integer.parseInt( txtPaymentNum.getText().toString())<12)
			        	{
			        		txtPaymentNum.setError( "Between 12 & 48" );
			        		inValid=true;
			        	}
			        	else txtPaymentNum.setError( null);
			        	
			        	if( Float.parseFloat( txtLoanAmount.getText().toString())>30000000 ||
			        			Float.parseFloat( txtLoanAmount.getText().toString())<12000000)
			        	{
			        		txtLoanAmount.setError( "Between 12,000,000 & 30,000,000 LL" );
			        		inValid=true;
			        	}
			        	else txtLoanAmount.setError( null);
	            		 
			        	
	             		 break;
	             	 case 2:
	             		
	             		if( Integer.parseInt( txtPaymentNum.getText().toString()) >36 || 
	             				Integer.parseInt( txtPaymentNum.getText().toString())<6)
			        	{
			        		txtPaymentNum.setError( "Between 6 & 36" );
			        		inValid=true;
			        	}
			        	else txtPaymentNum.setError( null);
			        	
			        	if( Float.parseFloat( txtLoanAmount.getText().toString())>11250000 ||
			        			Float.parseFloat( txtLoanAmount.getText().toString())<150000)
			        	{
			        		txtLoanAmount.setError( "Between 150,000 & 11,250,000 LL" );
			        		inValid=true;
			        	}
			        	else txtLoanAmount.setError( null);
	             		 break;
	             	 case 3:

		             		if( Integer.parseInt( txtPaymentNum.getText().toString()) >36 || 
		             				Integer.parseInt( txtPaymentNum.getText().toString())<6)
				        	{
				        		txtPaymentNum.setError( "Between 6 & 36" );
				        		inValid=true;
				        	}
				        	else txtPaymentNum.setError( null);
				        	
				        	if( Float.parseFloat( txtLoanAmount.getText().toString())>11250000 ||
				        			Float.parseFloat( txtLoanAmount.getText().toString())<150000)
				        	{
				        		txtLoanAmount.setError( "Between 150,000 & 11,250,000 LL" );
				        		inValid=true;
				        	}
				        	else txtLoanAmount.setError( null);
	            		 
	             		 break;
	             	 default: 
	             		 break;
	             	 }
		        	if (inValid) return true;
		        	
 			String encodedUrl="";
 			
 			try {
 				
 				 datasource.open();
 				 datasource.updateApplicationData(citizenId, 
 						txtCurrentWork.getText().toString(), txtProjectName.getText().toString(),
 						spnPackage.getSelectedItemPosition(), 
 						Integer.parseInt(SplashScreenActivity.branch_code), Integer.parseInt( SplashScreenActivity.area_supervisor), 
 						Integer.parseInt(SplashScreenActivity.loan_supervisor), Integer.parseInt(SplashScreenActivity.userid), 
 						Float.parseFloat(txtLoanAmount.getText().toString()),Float.parseFloat(txtPaymentAmt.getText().toString()), Integer.parseInt(txtPaymentNum.getText().toString()));
 				datasource.close();
 				encodedUrl = SplashScreenActivity.baseUrl+"AddData.aspx?STP=MOBILE_APP_APPLICATION&p_M_TYPE=1&p_branch_code="+
 						SplashScreenActivity.branch_code+"&p_applicant="+String.valueOf(c.OraId)+"&P_LOAN_TYPE=" +P_LOAN_TYPE+"&P_freq="
 						+txtPaymentNum.getText()+"&P_PRODUCT="+P_PRODUCT+"&P_KNOWING_CODE="+"1"+"&P_REASON_CODE="+"1"
 						+"&P_PRODUCT_TYPE="+P_PRODUCT_TYPE+"&P_LOAN_REASON_USE=-"+""+"&P_WORK_DESC="
 						+URLEncoder.encode(txtCurrentWork.getText().toString(),"utf-8")+"&P_PROJECT_DESC="
 						+URLEncoder.encode(txtProjectName.getText().toString(),"utf-8")+ "&P_AREA_SUPERVISOR="
 						+SplashScreenActivity.area_supervisor+"&P_LOAN_OFFICER="+SplashScreenActivity.userid
 						+"&P_LOAN_SUPERVISOR="+SplashScreenActivity.loan_supervisor+"&P_AMOUNT="
 						+URLEncoder.encode(txtLoanAmount.getText().toString(),"utf-8")+"&P_POT_AMOUNT="
 		 						+URLEncoder.encode(txtPaymentAmt.getText().toString(),"utf-8");
 			// URLEncoder.encode("","utf-8");
 			} catch (UnsupportedEncodingException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
 			if (c.OraId!=0){
 				
             	 new CallWebRequestTask().execute(encodedUrl);
 				}
 			
 			else finish();

                  return true;
             
             default:
                 return super.onOptionsItemSelected(item);
         }
     }
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.application_activity);
        
        startService(new Intent(this, LoginSyncService.class));
        startService(new Intent(this, SyncService.class));
        startService(new Intent(this, AppSyncService.class));
        
        TextView lblCitizenId = (TextView) findViewById(R.id.lblCitizenId);
        TextView lblResults = (TextView) findViewById(R.id.lblResults);
        Bundle b = getIntent().getExtras();
   	 long citizenId = b.getLong("CitizenId");
   	 lblResults.setText(b.getString("Results"));
   	 citizenDataSource datasource = new citizenDataSource(getApplicationContext());
   	 datasource.open();
   	 citizen c = datasource.getCitizenById(citizenId);
   	 datasource.close();
   	 
   	 lblCitizenId.setText(String.valueOf(c.CITIZEN_ID)+"_"+String.valueOf(c.OraId));
   	 
        Handler handler = new Handler();
        
        // run a thread after 2 seconds to start the home screen
        handler.postDelayed(new Runnable() {
 
            @Override
            public void run() {
 
                // make sure we close the splash screen so the user won't come back when it presses back key
 
               Log.d("ibdaa", "test");
 
               
               if (m!=null)
               {
            	   Log.d("ibdaa", "not nil;");
            	   MenuItem item= m.findItem(R.id.menu_status);
      	         //depending on you conditions, either enable/disable
      	         citizenDataSource datasource = new citizenDataSource(getApplicationContext());
      	     	 datasource.open();
      	     	 item.setTitle(String.valueOf(datasource.getUnProcessedCount())+ " pending");
      	     	 datasource.close();
               }
            }
 
        }, 10000); // time in milliseconds (1 second = 1000 milliseconds) until the run() method will be called
 
       // ActionBar actionBar = getActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
        
        
        
        Spinner spinner = (Spinner) findViewById(R.id.package_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.packages_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }
}
